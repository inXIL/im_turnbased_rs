use phf::phf_set;
use imgui::*;
use strum::EnumCount;
use strum_macros::{Display, EnumCount};
use std::cmp::*;

// where are the microtransactions and ads?!
// this game is WRONG!

mod support;

use std::collections::VecDeque;
use rand;

const BUTTON_SIZE: f32 = 200.0;
const UNIT_SIZE: f32 = 150.0;
const NUM_ROWS: usize = 4;
const NUM_COLS: usize = 2;
const RED: [f32; 4] = [0.7, 0.2, 0.2, 1.0];
const BLUE: [f32; 4] = [0.2, 0.2, 0.7, 1.0];


#[derive(Clone, Copy, Display, EnumCount)]
pub enum Class {
    Saber,
    Assassin,
    Caster,
    Berserker,
    Avalon,
    Excalibur,
    RuleBreaker,
    Joker,
    Suoyi,
    WoJiaoMeiMei,
    Legion,
    Godhand,
    None,
}

impl Class {
    // for randgen:
    fn from_usize(n: usize) -> Self {
        match n { // i'm horrible
            0 => Class::Saber,
            1 => Class::Assassin,
            2 => Class::Caster,
            3 => Class::Berserker,
            _ => panic!("AAAAA"),
        }
    }
    // Dear God, i couldn't make phf::phf_map work :{
    fn promotion_options(self) -> Vec<Class> {
        match self {
            Class::Saber => vec![Class::Excalibur, Class::Avalon],
            Class::Assassin => vec![Class::Suoyi, Class::WoJiaoMeiMei],
            Class::Caster => vec![Class::RuleBreaker, Class::Joker],
            Class::Berserker => vec![Class::Legion, Class::Godhand],
            _ => vec![],
        }
    }
}

#[derive(Clone)]
enum Target {
    Ally,
    Enemy,
}

enum State {
    Idle,
    Attack,
    Promote,
    Heal,
}

#[derive(Clone)]
struct Unit { // i dunno how to rust i guess :P
    class: Class,
    hp: i32,
    mhp: i32,
    exp: usize,
    maxexp: usize,
    atk: i32,
    def: i32,
    init: usize,
    ranged: bool,
    target: Target,
    multitarget: bool,
    defending: bool,
    dead: bool,
    exp_bounty: usize,
}

impl Unit {
    fn new(class: Class) -> Self {
        match class {
            Class::Saber => Self {
                class,
                hp: 10,
                mhp: 5,
                exp: 0,
                maxexp: 10,
                atk: 3,
                def: 2,
                init: 3,
                ranged: false,
                target: Target::Enemy,
                multitarget: false,
                defending: false,
                dead: false,
                exp_bounty: 7,
            },
            Class::Caster => Self {
                class,
                hp: 20,
                mhp: 20,
                exp: 0,
                maxexp: 10,
                atk: 7,
                def: 2,
                init: 7,
                ranged: false,
                target: Target::Ally,
                multitarget: false,
                defending: false,
                dead: false,
                exp_bounty: 13,
            },
            Class::Assassin => Self {
                class,
                hp: 10,
                mhp: 10,
                exp: 0,
                maxexp: 10,
                atk: 10,
                def: 2,
                init: 10,
                ranged: true,
                target: Target::Enemy,
                multitarget: false,
                defending: false,
                dead: false,
                exp_bounty: 8,
            },
            Class::Berserker => Self {
                class,
                hp: 7,
                mhp: 7,
                exp: 0,
                maxexp: 10,
                atk: 6,
                def: 4,
                init: 20,
                ranged: false,
                target: Target::Enemy,
                multitarget: true,
                defending: false,
                dead: false,
                exp_bounty: 11
            },
            Class::Excalibur => Self {
                class,
                hp: 7,
                mhp: 7,
                exp: 0,
                maxexp: 10,
                atk: 11,
                def: 6,
                init: 20,
                ranged: false,
                target: Target::Enemy,
                multitarget: true,
                defending: false,
                dead: false,
                exp_bounty: 11
            },
            Class::Avalon => Self {
                class,
                hp: 7,
                mhp: 7,
                exp: 0,
                maxexp: 10,
                atk: 11,
                def: 10,
                init: 20,
                ranged: false,
                target: Target::Enemy,
                multitarget: true,
                defending: false,
                dead: false,
                exp_bounty: 11
            },
            Class::Suoyi => Self {
                class,
                hp: 7,
                mhp: 7,
                exp: 0,
                maxexp: 10,
                atk: 11,
                def: 10,
                init: 20,
                ranged: false,
                target: Target::Enemy,
                multitarget: true,
                defending: false,
                dead: false,
                exp_bounty: 11
            },
            Class::WoJiaoMeiMei => Self {
                class,
                hp: 7,
                mhp: 7,
                exp: 0,
                maxexp: 10,
                atk: 11,
                def: 10,
                init: 20,
                ranged: false,
                target: Target::Enemy,
                multitarget: true,
                defending: false,
                dead: false,
                exp_bounty: 11
            },
            Class::RuleBreaker => Self {
                class,
                hp: 7,
                mhp: 7,
                exp: 0,
                maxexp: 10,
                atk: 11,
                def: 10,
                init: 20,
                ranged: false,
                target: Target::Enemy,
                multitarget: true,
                defending: false,
                dead: false,
                exp_bounty: 11
            },
            Class::Joker => Self {
                class,
                hp: 7,
                mhp: 7,
                exp: 0,
                maxexp: 10,
                atk: 11,
                def: 10,
                init: 20,
                ranged: false,
                target: Target::Enemy,
                multitarget: true,
                defending: false,
                dead: false,
                exp_bounty: 11
            },
            Class::Godhand => Self {
                class,
                hp: 7,
                mhp: 7,
                exp: 0,
                maxexp: 10,
                atk: 11,
                def: 10,
                init: 20,
                ranged: false,
                target: Target::Enemy,
                multitarget: true,
                defending: false,
                dead: false,
                exp_bounty: 11
            },
            Class::Legion => Self {
                class,
                hp: 7,
                mhp: 7,
                exp: 0,
                maxexp: 10,
                atk: 11,
                def: 5,
                init: 20,
                ranged: false,
                target: Target::Enemy,
                multitarget: true,
                defending: false,
                dead: false,
                exp_bounty: 11
            },
            _ => panic!("NOOOO"),
        }
    }
    fn random_new() -> Self {
        Unit::new(Class::from_usize(rand::random::<usize>() % 4))
    }
    fn defend(&mut self) {
        self.defending = true;
    }
    fn update(&mut self) {
        self.defending = false;
    }
    fn try_promote(&mut self, exp: usize) -> bool {
        self.exp += exp;
        self.exp >= self.maxexp
    }
    fn take_damage(&mut self, damage: i32) {
        let mut damage = damage;
        dbg!(damage);
        if self.defending {
            damage /= 2;
        }
        damage = (damage as f32 * (1.0 - self.def as f32 / 100.0)) as i32;
        dbg!(damage);
        self.hp -= damage;
        if self.hp <= 0 {
            self.dead = true;
        }
    }
    fn attack_damage(&self) -> i32 {
        self.atk
    }
    fn is_multitarget(&self) -> bool {
        self.multitarget
    }
    fn is_dead(&self) -> bool {
        self.dead
    }
    fn exp_bounty(&self) -> usize {
        self.exp_bounty
    }
    fn render(&self, pos: (usize, usize), ui: &mut Ui, current_turn: bool, can_be_attacked: bool) {

        let color = ui.push_style_color(StyleColor::WindowBg, if pos.0 < NUM_ROWS / 2 {RED} else {BLUE});
        Window::new(&im_str!("Unit{}{}", pos.0, pos.1)) // why the heck is it &im_str! here and not & like above
        .position([2.0 * BUTTON_SIZE + pos.1 as f32 * UNIT_SIZE, pos.0 as f32 * UNIT_SIZE], Condition::Always)
        .size([UNIT_SIZE, UNIT_SIZE], Condition::Always)
        .title_bar(false)
        .resizable(false)
        .build(ui, || {
            ui.text(format!("{}", self.class));
            ProgressBar::new(self.hp as f32 / self.mhp as f32)
            .size([ui.window_content_region_width(), 12.0])
            .overlay_text(&im_str!("hp:{}/{}", self.hp, self.mhp))
            .build(&ui);
            ProgressBar::new(self.exp as f32 / self.maxexp as f32)
            .size([ui.window_content_region_width(), 12.0])
            .overlay_text(&im_str!("exp:{}/{}", self.exp, self.maxexp))
            .build(&ui);
            ui.text(format!("atk: {}\ndef: {}\ninit: {} ", self.atk, self.def, self.init));
            if self.defending {
                ui.bullet_text(im_str!("DEFENDING"));
            }
            if current_turn {
                ui.bullet_text(im_str!("CURRENT TURN"));
            }
            if can_be_attacked {
                ui.bullet_text(im_str!("CAN BE ATTACKED"));
            }
        });
        color.pop(&ui);
    }
    fn render_promotion_window(&self, ui: &mut Ui) -> Class {
        let mut ret = Class::None;
        Window::new(&im_str!("Promotion of {}", self.class))
        .build(ui, || {
            ui.text(&im_str!("Promotion of {}", self.class));
            let opts = self.class.promotion_options();
            for e in opts {
                if ui.button(&im_str!("{}", e), [100.0, 20.0]) {
                    ret = e;
                }
            }
        });
        ret
    }
}

enum Action {
    Attack,
    Defend,
    Heal,
    None
}

fn build_action_choice_ui(ui: &mut Ui) -> Action {
    let mut attack_taken = false;
    build_attack_window(ui, &mut attack_taken);
    let mut defence_taken = false;
    build_defence_window(ui, &mut defence_taken);
    if attack_taken && !defence_taken {
        Action::Attack
    } else if !attack_taken && defence_taken {
        Action::Defend
    } else if !attack_taken && !defence_taken {
        Action::None
    } else {
        panic!("AAAA");
    }
}

fn build_attack_window(ui: &mut Ui, attack_taken: &mut bool) {
    prepare_action_window(im_str!("Attack"),[0.0, 0.0]).build(ui, || {
        if ui.button(im_str!("Attack"), ui.window_size()) {
            *attack_taken = true;
        }
    });
}

fn build_defence_window(ui: &mut Ui, defence_taken: &mut bool) {
    prepare_action_window(im_str!("Defend"), [BUTTON_SIZE, 0.0]).build(ui, || {
        if ui.button(im_str!("Defend"), ui.window_size()) {
            *defence_taken = true;
        }
    });
}

fn prepare_action_window<'a>(string: &'a ImStr, pos: [f32; 2]) -> imgui::Window<'a> {
    Window::new(string)
    .position(pos, Condition::Always)
    .size([BUTTON_SIZE, BUTTON_SIZE], Condition::Always)
    .title_bar(false)
    .resizable(false)
    .scroll_bar(false)
}



struct TurnQueue{queue: VecDeque<(usize, usize)>}

impl TurnQueue {
    fn new(units: &UnitGrid) -> Self {
        let mut taken = vec![vec![false; NUM_COLS]; NUM_ROWS];
        let mut cnt_taken = 0;
        let mut queue = VecDeque::<(usize, usize)>::new();
        // sorts by initiative:
        while cnt_taken != NUM_ROWS * NUM_COLS {
            let mut max_init = 0;
            let mut max_init_id = (0, 0);
            for row in 0..NUM_ROWS {
                for col in 0..NUM_COLS {
                    let unit = units.unit_ref((row, col)).unwrap();
                    if  !taken[row][col] && unit.init > max_init {
                        max_init = unit.init;
                        max_init_id = (row, col);
                    }
                }
            }
            taken[max_init_id.0][max_init_id.1] = true;
            cnt_taken += 1;
            queue.push_back(max_init_id);
        }
        Self{queue}
    }
    fn discard_dead(&mut self, grid: &UnitGrid) {
        while self.queue.len() > 0 && {
            let next = self.next();
            grid.unit_ref(next).is_none() ||
            grid.unit_ref(next).unwrap().hp <= 0
        } {
            self.queue.pop_front();
        }
        'm: loop {
            for i in 0..self.queue.len() {
                let e = self.queue[i];
                if grid.unit_ref(e).is_none() {
                    self.queue.remove(i);
                    continue 'm;
                }
            }
            break;
        }

    }
    fn wrap(&mut self) {
        let mut pop = self.queue.pop_front().unwrap();
        self.queue.push_back(pop);
    }
    fn next(&self) -> (usize, usize) {
        *self.queue.front().unwrap()
    }
    fn render_queue_ui(&self, ui: &mut Ui, grid: &UnitGrid) {
        const QUEUE_HEIGHT: f32 = 50.0;
        for i in 0..self.queue.len() {
            let (row, col) = self.queue[i];
            let color = ui.push_style_color(StyleColor::WindowBg, if row <= 1 {RED} else {BLUE});
            Window::new(&im_str!("UnitQ{}", i))
            .position([2.0 * BUTTON_SIZE + NUM_COLS as f32 * UNIT_SIZE, i as f32 * QUEUE_HEIGHT], Condition::Always)
            .size([BUTTON_SIZE, QUEUE_HEIGHT], Condition::Always)
            .bg_alpha(0.5)
            .title_bar(false)
            .resizable(false)
            .build(ui, || {
                ui.text(format!("{}", grid.unit_ref((row, col)).unwrap().class));
            });
            color.pop(&ui);
        }
    }
}

struct UnitGrid {
    units: Vec<Vec<Option<Box<Unit>>>>,
}

impl UnitGrid {
    fn random_grid() -> Self {
        let mut units = vec![vec![None; NUM_COLS]; NUM_ROWS];
        for row in &mut units {
            for col in row {
                *col = Some(Box::new(Unit::random_new()));
            }
        }
        Self {
            units
        }
    }
    fn unit_ref(&self, pos: (usize, usize)) -> Option<&Box<Unit>> {
        self.units[pos.0][pos.1].as_ref()
    }
    fn unit_mut(&mut self, pos: (usize, usize)) -> Option<&mut Box<Unit>> {
        self.units[pos.0][pos.1].as_mut()
    }
    fn enemy_units_if_some(&self, pos: (usize, usize)) -> Vec<(usize, usize)> {
        let start_row = (pos.0 / (self.units.len() / 2) + 1) % 2 * (self.units.len() / 2);
        let mut ret = vec![];
        for i in 0..(self.units.len() / 2) {
            for j in 0..self.units[0].len() {
                if self.unit_ref((start_row + i, j)).is_some() {
                    ret.push((start_row + i, j));
                }
            }
        }
        ret
    }
    fn enemy_frontline_units_if_some(&self, pos:(usize, usize)) -> Vec<(usize, usize)> {
        let mut row = (pos.0 / (self.units.len() / 2) + 1) % 2 * (self.units.len() / 2);
        if row == 0 {
            row = self.units.len() / 2 - 1
        }
        let mut ret = vec![];
        for i in 0..self.units[0].len() {
            if self.unit_ref((row, i)).is_some() {
                ret.push((row, i));
            }
        }
        ret
    }
    fn enemy_backline_units_if_some(&self, pos: (usize, usize)) -> Vec<(usize, usize)> {
        let mut row = (pos.0 / (self.units.len() / 2) + 1) % 2 * (self.units.len() / 2);
        if row != 0 {
            row = self.units.len() - 1
        }
        let mut ret = vec![];
        for i in 0..self.units[0].len() {
            if self.unit_ref((row, i)).is_some() {
                ret.push((row, i));
            }
        }
        ret
    }
    fn ally_units(&self, pos: (usize, usize)) -> Vec<(usize, usize)> {
        let start_row = pos.0 / (self.units.len() / 2);
        let mut ret = vec![];
        for i in 0..(self.units.len() / 2) {
            for j in 0..self.units[0].len() {
                if self.unit_ref((i, j)).is_some() {
                    ret.push((i, j));
                }
            }
        }
        ret
    }
    fn render_unit_if_some(&self, pos: (usize, usize), ui: &mut Ui, current_turn: bool, can_be_attacked: bool) {
        if let Some(unit) = self.unit_ref(pos) {
            unit.render(pos, ui, current_turn, can_be_attacked);
        }
    }
    fn render_alive_units(&self, ui: &mut Ui, current_pos: (usize, usize), attacked_pos: &Vec<(usize, usize)>) {
        for i in 0..self.units.len() {
            for j in 0..self.units[0].len() {
                self.render_unit_if_some((i, j), ui, current_pos == (i, j), attacked_pos.contains(&(i, j)));
            }
        }
    }
    fn render_unit_group_for_attack(&self, ui: &mut Ui, group: &Vec<(usize, usize)>) -> Option<(usize, usize)> {
        let mut ret = None;
        for &pos in group {
            Window::new(&im_str!("Attack{}{}", pos.0, pos.1)) // why the heck is it &im_str! here and not & like above
            .position([2.0 * BUTTON_SIZE + pos.1 as f32 * UNIT_SIZE, pos.0 as f32 * UNIT_SIZE], Condition::Always)
            .size([UNIT_SIZE / 2.0, UNIT_SIZE / 2.0], Condition::Always)
            .title_bar(false)
            .resizable(false)
            .bg_alpha(0.3)
            .build(ui, || {
                ui.text("Attack?");
                if ui.is_window_hovered() && ui.is_mouse_clicked(MouseButton::Left) {
                    ret = Some(pos);
                }
            });
        }
        ret
    }
    fn promote_unit_if_some(&mut self, pos: (usize, usize), into_class: Class) {
        if let Some(unit) = self.unit_mut(pos) {
            self.units[pos.0][pos.1] = Some(Box::new(Unit::new(into_class)));
        }
    }
    fn damage_unit_if_some_and_exp(&mut self, pos: (usize, usize), damage: i32) -> usize {
        if let Some(unit) = self.unit_mut(pos) {
            unit.take_damage(damage);
            if unit.is_dead() {
                let exp = unit.exp_bounty();
                self.units[pos.0][pos.1] = None;
                exp
            } else {
                0
            }
        } else {
            0
        }
    }
    fn targets_for_ranged(&self, pos: (usize, usize)) -> Vec<(usize, usize)> {
        self.enemy_backline_units_if_some(pos)
    }
    fn targets_for_melee(&self, pos: (usize, usize)) -> Vec<(usize, usize)> {
        self.enemy_frontline_units_if_some(pos)
    }
    fn damage_units_and_collect_exp(&mut self, pos: &Vec<(usize, usize)>, damage: i32) -> usize {
        let mut exp = 0;
        for &e in pos {
            exp += self.damage_unit_if_some_and_exp(e, damage);
        }
        exp
    }
    fn can_be_attacked_group(&self, pos: (usize, usize)) -> Vec<(usize, usize)> {
        if self.unit_ref(pos).unwrap().multitarget {
            self.enemy_units_if_some(pos)
        } else {
            if self.unit_ref(pos).unwrap().ranged {
                self.targets_for_ranged(pos)
            } else {
                self.targets_for_melee(pos)
            }
        }
    }
}

fn main() {
    let system = support::init(file!());

    let mut unit_grid = UnitGrid::random_grid();

    let mut queue = TurnQueue::new(&unit_grid);

    let mut state = State::Idle;
    system.main_loop(move |_, mut ui| {

        // do some cleanup:
        queue.discard_dead(&unit_grid);

        queue.render_queue_ui(&mut ui, &unit_grid);

        let next_unit = queue.next();
        unit_grid.unit_mut(next_unit).unwrap().update();
        let can_be_attacked = unit_grid.can_be_attacked_group(next_unit);

        unit_grid.render_alive_units(&mut ui, next_unit, &can_be_attacked);

        match state {
            State::Idle => {
                match build_action_choice_ui(&mut ui) {
                    Action::Attack => {
                        state = State::Attack;
                    },
                    Action::Heal => {
                        state = State::Heal;
                    },
                    Action::Defend => {
                        unit_grid.unit_mut(next_unit).unwrap().defend();
                        queue.wrap();
                    },
                    Action::None => (),
                }
            },
            State::Attack => {
                if unit_grid.unit_ref(next_unit).unwrap().is_multitarget() {
                    let v = unit_grid.enemy_units_if_some(next_unit);
                    let damage = unit_grid.unit_ref(next_unit).as_ref().unwrap().attack_damage();
                    let exp = unit_grid.damage_units_and_collect_exp(&v, damage);
                    if /*unit_grid.unit_mut(next_unit).is_some()
                    &&*/ unit_grid.unit_mut(next_unit).unwrap().try_promote(exp)
                    && unit_grid.unit_mut(next_unit).unwrap().class.promotion_options().len() > 0 {
                        state = State::Promote;
                    } else {
                        queue.wrap();
                        state = State::Idle;
                    }
                } else {
                    if let Some(pos) = unit_grid.render_unit_group_for_attack(&mut ui, &can_be_attacked) {
                        let dmg = unit_grid.unit_ref(next_unit).unwrap().attack_damage();
                        let exp = unit_grid.damage_unit_if_some_and_exp(pos, dmg);
                        if unit_grid.unit_mut(next_unit).unwrap().try_promote(exp) {
                            state = State::Promote;
                        } else {
                            queue.wrap();
                            state = State::Idle;
                        }
                    }
                }
            },
            State::Heal => {

            },
            State::Promote => {
                match unit_grid.unit_mut(next_unit).unwrap().render_promotion_window(&mut ui) {
                    Class::None => (),
                    (class) => {
                        unit_grid.promote_unit_if_some(next_unit, class);
                        queue.wrap();
                        state = State::Idle;
                    }
                }
            },
        }
    });
}

